#vim: set fileencoding=utf-8
with open('README.rst') as f:
    long_description = f.read()
from setuptools import setup

setup(
    name='pyPEG',
    version='1.5',
    author=u'STXNext Szymon Pyżalski',
    author_email=u'szymon.pyzalski@stxnext.pl',
    packages=['pyPEG'],
    description='PEG parser for python',
    long_description=long_description,
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Topic :: Text Processing',
    ],
)
