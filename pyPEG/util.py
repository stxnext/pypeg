import sys
import re

def u(text):
    if isinstance(text, BaseException):
        text = text.args[0]
    if type(text) is unicode:
        return text
    if isinstance(text, str):
        if sys.stdin.encoding:
            return codecs.decode(text, sys.stdin.encoding)
        else:
            return codecs.decode(text, "utf-8")
    return unicode(text)

def skip(skipper, text, skipWS, skipComments):
    if skipWS:
        t = text.lstrip()
    else:
        t = text
    if skipComments:
        try:
            while True:
                skip, t = skipper.parseLine(t, skipComments, [], skipWS, None)
                if skipWS:
                    t = t.lstrip()
        except: pass
    return t

valid_regex = set()

def validate_regex(regex):
    """Tests whether this regex can be used in parser. Currently rejects
regexes that match empty string"""
    if regex not in valid_regex and regex.match(''):
        raise ValueError(
            'Regular expression %s matches empty string' % regex.pattern
        )
    valid_regex.add(regex)

word_regex = re.compile(ur"\w+")
rest_regex = re.compile(ur".*")
RegEx = type(word_regex)
