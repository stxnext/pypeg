"""Parser object definition"""

import logging
import re
import sys, codecs
import exceptions

from pyPEG.rules import Name, Symbol, Rule
from pyPEG.util import u, skip, word_regex, rest_regex, validate_regex

log = logging.getLogger('pyPEG')


class keyword(unicode): pass
class code(unicode): pass
class ignore(object):
    def __init__(self, regex_text, *args):
        self.regex = re.compile(regex_text, *args)
        validate_regex(self.regex)

class _and(object):
    def __init__(self, something):
        self.obj = something

class _not(_and): pass


class parser(object):
    def __init__(self, another = False, p = False):
        self.restlen = -1 
        if not(another):
            self.skipper = parser(True, p)
            self.skipper.packrat = p
        else:
            self.skipper = self
        self.lines = None
        self.textlen = 0
        self.memory = {}
        self.packrat = p


    def parseLine(
        self, textline, pattern, resultSoFar = [], skipWS = True,
        skipComments = None
    ):
        """textline:       text to parse
pattern:        pyPEG language description
resultSoFar:    parsing result so far (default: blank list [])
skipWS:         Flag if whitespace should be skipped (default: True)
skipComments:   Python functions returning pyPEG for matching comments

returns:        pyAST, textrest

raises:         SyntaxError(reason) if textline is detected not being in language
                described by pattern

                SyntaxError(reason) if pattern is an illegal language description"""
        name = None
        _textline = textline
        _pattern = pattern

        def R(result, text, format_result):
            if __debug__:
                try:
                    if _pattern.__name__ != "comment":
                        log.debug(u"match: " + _pattern.__name__)
                except: pass

            if self.restlen == -1:
                self.restlen = len(text)
            else:
                self.restlen = min(self.restlen, len(text))
            res = resultSoFar
            if format_result:
                res.append(format_result(result))
            elif name and result:
                name.line = self.lineNo()
                res.append(Symbol(name, result))
            elif name:
                name.line = self.lineNo()
                res.append(Symbol(name, []))
            elif result:
                if type(result) is type([]):
                    res.extend(result)
                else:
                    res.extend([result])
            if self.packrat:
                self.memory[(len(_textline), id(_pattern))] = (res, text)
            return res, text

        def syntaxError():
            if self.packrat:
                self.memory[(len(_textline), id(_pattern))] = False
            raise SyntaxError()

        if self.packrat:
            try:
                result = self.memory[(len(textline), id(pattern))]
                if result:
                    return result
                else:
                    raise SyntaxError()
            except: pass
        format_result = None
        if isinstance(pattern, type):
            pattern = pattern()
        if isinstance(pattern, Rule):
            pattern_name = type(pattern).__name__
            log.debug(u'testing with {0}: {1}'.format(
                pattern_name, textline[:40]
            ))
            name = Name(pattern_name)
            format_result = pattern.format_result
            pattern = pattern.pattern
            if callable(pattern):
                pattern = pattern()
        elif callable(pattern):
            if __debug__:
                try:
                    if pattern.__name__ != "comment":
                        log.debug(u"testing with " + pattern.__name__ + u": " + textline[:40])
                except: pass

            if pattern.__name__[0] != "_":
                name = Name(pattern.__name__)

            pattern = pattern()
            if callable(pattern):
                pattern = (pattern,)
        pattern_type = type(pattern)

        text = skip(self.skipper, textline, skipWS, skipComments)

        if pattern_type is str or pattern_type is unicode:
            if text[:len(pattern)] == pattern:
                text = skip(self.skipper, text[len(pattern):], skipWS, skipComments)
                return R(None, text, format_result)
            else:
                syntaxError()

        elif pattern_type is keyword:
            m = word_regex.match(text)
            if m:
                if m.group(0) == pattern:
                    text = skip(self.skipper, text[len(pattern):], skipWS, skipComments)
                    return R(None, text, format_result)
                else:
                    syntaxError()
            else:
                syntaxError()

        elif pattern_type is _not:
            try:
                r, t = self.parseLine(text, pattern.obj, [], skipWS, skipComments)
            except:
                return resultSoFar, textline
            syntaxError()

        elif pattern_type is _and:
            r, t = self.parseLine(text, pattern.obj, [], skipWS, skipComments)
            return resultSoFar, textline

        elif pattern_type is type(word_regex) or pattern_type is ignore:
            validate_regex(pattern)
            if pattern_type is ignore:
                pattern = pattern.regex
            m = pattern.match(text)
            if m:
                text = skip(self.skipper, text[len(m.group(0)):], skipWS, skipComments)
                if pattern_type is ignore:
                    return R(None, text, format_result)
                else:
                    return R(m.group(0), text, format_result)
            else:
                syntaxError()

        elif pattern_type is tuple:
            result = []
            n = 1
            for p in pattern:
                if type(p) is type(0):
                    n = p
                else:
                    if n>0:
                        for i in range(n):
                            result, text = self.parseLine(text, p, result, skipWS, skipComments)
                    elif n==0:
                        if text == "":
                            pass
                        else:
                            try:
                                newResult, newText = self.parseLine(text, p, result, skipWS, skipComments)
                                result, text = newResult, newText
                            except SyntaxError:
                                pass
                    elif n<0:
                        found = False
                        while True:
                            try:
                                newResult, newText = self.parseLine(text, p, result, skipWS, skipComments)
                                result, text, found = newResult, newText, True
                            except SyntaxError:
                                break
                        if n == -2 and not(found):
                            syntaxError()
                    n = 1
            return R(result, text, format_result)

        elif pattern_type is list:
            result = []
            found = False
            for p in pattern:
                try:
                    result, text = self.parseLine(text, p, result, skipWS, skipComments)
                    found = True
                except SyntaxError:
                    pass
                if found:
                    break
            if found:
                return R(result, text, format_result)
            else:
                syntaxError()

        else:
            raise SyntaxError(u"illegal type in grammar: " + u(pattern_type))

    def lineNo(self):
        if not(self.lines): return u""
        if self.restlen == -1: return u""
        parsed = self.textlen - self.restlen

        left, right = 0, len(self.lines)

        while True:
            mid = int((right + left) / 2)
            if self.lines[mid][0] <= parsed:
                try:
                    if self.lines[mid + 1][0] >= parsed:
                        try:
                            return u(self.lines[mid + 1][1]) + u":" + u(self.lines[mid + 1][2])
                        except:
                            return u""
                    else:
                        left = mid + 1
                except:
                    try:
                        return u(self.lines[mid + 1][1]) + u":" + u(self.lines[mid + 1][2])
                    except:
                        return u""
            else:
                right = mid - 1
            if left > right:
                return u""
