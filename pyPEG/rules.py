from pyPEG.util import RegEx, validate_regex

class Symbol(list):
    def __init__(self, name, what):
        self.__name__ = name
        self.append(name)
        self.what = what
        self.append(what)
    def __call__(self):
        return self.what
    def __unicode__(self):
        return u'Symbol(' + repr(self.__name__) + ', ' + repr(self.what) + u')'
    def __repr__(self):
        return unicode(self)

class Name(unicode):
    def __init__(self, *args):
        self.line = 0
        self.file = u""

class Rule(object):
    def __init__(self):
        self.type_ = type(self.pattern)
        if isinstance(self.pattern, RegEx):
            validate_regex(self.pattern)

    def format_result(self, result, file_, line_no):
        name = Name(type(self).__name__)
        name.lineNo = line_no
        name.file = file_
        return Symbol(name, result)
