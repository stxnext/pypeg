--------------------
pyPEG
--------------------

This is a fork of pyPEG by Volker Birk

Original version of the package is documented here: http://fdik.org/pyPEG/

Changes:
----------

 + Preparing the script to be distributed via setuptools
 + Parser validates patterns and rejects them if they match empty string
 + Parser uses python logging framework to log (it wrote to stderr previously)
